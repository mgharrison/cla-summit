# CLA Demo Application

This is a simple application that takes a reading and displays the waveform to the user.

# Contributing

All code must be written in LabVIEW 2017.

All UI controls must use Flat Design principles.